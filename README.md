# Project details

Original project with lectures and homeworks
https://github.com/cdeler/epam_python_autumn_2020

hw directory contains tasks with it's solutions
tests directory contains tests and additional file for the provided solutions

$ pip install -r requirements.txt
